<?php

namespace App\Entity;

use App\Repository\StatusDataRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=StatusDataRepository::class)
 */
class StatusData implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Beacon::class, inversedBy="statusData")
     * @ORM\JoinColumn(nullable=false)
     */
    private $beacon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uptime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rssi;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeacon(): ?Beacon
    {
        return $this->beacon;
    }

    public function setBeacon(?Beacon $beacon): self
    {
        $this->beacon = $beacon;

        return $this;
    }

    public function getUptime(): ?string
    {
        return $this->uptime;
    }

    public function setUptime(?string $uptime): self
    {
        $this->uptime = $uptime;

        return $this;
    }

    public function getRssi(): ?string
    {
        return $this->rssi;
    }

    public function setRssi(?string $rssi): self
    {
        $this->rssi = $rssi;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            "uptime" => $this->getUptime(),
            "rssi" => $this->getRssi(),
            "createdAt" => $this->getCreatedAt()
        ];
    }
}
