<?php

namespace App\Entity;

use App\Repository\BeaconRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BeaconRepository::class)
 */
class Beacon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addressType;

    /**
     * @ORM\OneToMany(targetEntity=AccelData::class, mappedBy="beacon", orphanRemoval=true)
     */
    private $accelData;

    /**
     * @ORM\OneToMany(targetEntity=GyroData::class, mappedBy="beacon", orphanRemoval=true)
     */
    private $gyroData;

    /**
     * @ORM\OneToMany(targetEntity=StatusData::class, mappedBy="beacon", orphanRemoval=true)
     */
    private $statusData;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = false;

    public function __construct()
    {
        $this->accelData = new ArrayCollection();
        $this->gyroData = new ArrayCollection();
        $this->statusData = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressType(): ?string
    {
        return $this->addressType;
    }

    public function setAddressType(?string $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }

    /**
     * @return Collection|AccelData[]
     */
    public function getAccelData(): Collection
    {
        return $this->accelData;
    }

    public function addAccelData(AccelData $accelData): self
    {
        if (!$this->accelData->contains($accelData)) {
            $this->accelData[] = $accelData;
            $accelData->setBeacon($this);
        }

        return $this;
    }

    public function removeAccelData(AccelData $accelData): self
    {
        if ($this->accelData->removeElement($accelData)) {
            // set the owning side to null (unless already changed)
            if ($accelData->getBeacon() === $this) {
                $accelData->setBeacon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GyroData[]
     */
    public function getGyroData(): Collection
    {
        return $this->gyroData;
    }

    public function addGyroData(GyroData $gyroData): self
    {
        if (!$this->gyroData->contains($gyroData)) {
            $this->gyroData[] = $gyroData;
            $gyroData->setBeacon($this);
        }

        return $this;
    }

    public function removeGyroData(GyroData $gyroData): self
    {
        if ($this->gyroData->removeElement($gyroData)) {
            // set the owning side to null (unless already changed)
            if ($gyroData->getBeacon() === $this) {
                $gyroData->setBeacon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatusData[]
     */
    public function getStatusData(): Collection
    {
        return $this->statusData;
    }

    public function addStatusData(StatusData $statusData): self
    {
        if (!$this->statusData->contains($statusData)) {
            $this->statusData[] = $statusData;
            $statusData->setBeacon($this);
        }

        return $this;
    }

    public function removeStatusData(StatusData $statusData): self
    {
        if ($this->statusData->removeElement($statusData)) {
            // set the owning side to null (unless already changed)
            if ($statusData->getBeacon() === $this) {
                $statusData->setBeacon(null);
            }
        }

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }
}
