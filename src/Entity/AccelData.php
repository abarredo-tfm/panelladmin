<?php

namespace App\Entity;

use App\Repository\AccelDataRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=AccelDataRepository::class)
 */
class AccelData implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Beacon::class, inversedBy="accelData")
     * @ORM\JoinColumn(nullable=false)
     */
    private $beacon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $x;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $y;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $z;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeacon(): ?Beacon
    {
        return $this->beacon;
    }

    public function setBeacon(?Beacon $beacon): self
    {
        $this->beacon = $beacon;

        return $this;
    }

    public function getX(): ?string
    {
        return $this->x;
    }

    public function setX(?string $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getY(): ?string
    {
        return $this->y;
    }

    public function setY(?string $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getZ(): ?string
    {
        return $this->z;
    }

    public function setZ(?string $z): self
    {
        $this->z = $z;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            "x"=> $this->getX(),
            "y"=> $this->getY(),
            "z"=> $this->getZ(),
            "createdAt"=> $this->getCreatedAt()
        ];
    }
}
