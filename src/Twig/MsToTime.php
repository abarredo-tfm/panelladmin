<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MsToTime extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('msToTime', [$this, 'msToTime']),
        ];
    }

    public function msToTime(int $ms)
    {

        $milliseconds = intval($ms % 1000);
        $seconds = floor(($ms / 1000) % 60);
        $minutes = floor(($ms / (1000 * 60)) % 60);
        $hours = floor(($ms / (1000 * 60 * 60)) % 24);

        $hours = ($hours < 10) ? "0" . $hours : $hours;
        $minutes = ($minutes < 10) ? "0" . $minutes : $minutes;
        $seconds = ($seconds < 10) ? "0" . $seconds : $seconds;

        return $hours . ":" . $minutes . ":" . $seconds . "." . $milliseconds;
    }
}