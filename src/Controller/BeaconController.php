<?php

namespace App\Controller;

use App\Entity\AccelData;
use App\Entity\Beacon;
use App\Entity\GyroData;
use App\Entity\StatusData;
use App\Form\BeaconType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BeaconController extends AbstractController
{
    private $em;

    /**
     * BeaconController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/beacon/list", name="beacons_list")
     */
    public function list(): Response
    {
        $beacons = $this->em->getRepository(Beacon::class)->findAll();
        return $this->render('beacons.html.twig', [
            'beacons' => $beacons,
        ]);
    }

    /**
     * @Route("/beacon/new", name="beacon_new")
     */
    public function new(Request $request): Response
    {
        $beacon = new Beacon();

        $form = $this->createForm(BeaconType::class, $beacon);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $beacon = $form->getData();

            $this->em->persist($beacon);
            $this->em->flush();

            return $this->redirectToRoute('beacons_list');
        }

        return $this->render('new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/beacon/edit/{beacon}", name="beacon_edit")
     */
    public function edit(Request $request, Beacon $beacon): Response
    {
        $form = $this->createForm(BeaconType::class, $beacon);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $beacon = $form->getData();

            $this->em->persist($beacon);
            $this->em->flush();

            return $this->redirectToRoute('beacons_list');
        }

        return $this->render('new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/beacon/remove/{beacon}", name="beacon_remove")
     */
    public function remove(Request $request, Beacon $beacon): Response
    {
        $this->em->remove($beacon);
        $this->em->flush();

        return $this->redirectToRoute('beacons_list');
    }

    /**
     * @Route("/beacon/{beacon}", name="beacon")
     */
    public function index(Beacon $beacon = null): Response
    {
        $gyroData = $this->em->getRepository(GyroData::class)->findOneBy(["beacon" => $beacon], ["id" => "DESC"]);
        $accelData = $this->em->getRepository(AccelData::class)->findOneBy(["beacon" => $beacon], ["id" => "DESC"]);
        $statusData = $this->em->getRepository(StatusData::class)->findOneBy(["beacon" => $beacon], ["id" => "DESC"]);

        $response = [];
        $response["id"] = $beacon->getId();
        $response["gyro"] = $gyroData;
        $response["accel"] = $accelData;
        $response["status"] = $statusData;

        return $this->json($response);
    }
}
