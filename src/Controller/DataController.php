<?php

namespace App\Controller;

use App\Entity\AccelData;
use App\Entity\GyroData;
use App\Entity\StatusData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends AbstractController
{
    private $em;

    /**
     * BeaconController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/data/gyro", name="data_gyro")
     */
    public function gyro(): Response
    {
        $dataHistory = $this->em->getRepository(GyroData::class)->findAll();
        return $this->render('dataHistory.html.twig', [
            'dataHistory' => $dataHistory,
        ]);
    }

    /**
     * @Route("/data/accel", name="data_accel")
     */
    public function accel(): Response
    {
        $dataHistory = $this->em->getRepository(AccelData::class)->findAll();
        return $this->render('dataHistory.html.twig', [
            'dataHistory' => $dataHistory,
        ]);
    }

    /**
     * @Route("/data/status", name="data_status")
     */
    public function status(): Response
    {
        $dataHistory = $this->em->getRepository(StatusData::class)->findAll();
        return $this->render('statusDataHistory.html.twig', [
            'dataHistory' => $dataHistory,
        ]);
    }
}
