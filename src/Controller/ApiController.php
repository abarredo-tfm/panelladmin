<?php

namespace App\Controller;

use App\Entity\AccelData;
use App\Entity\Beacon;
use App\Entity\GyroData;
use App\Entity\StatusData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractController
{

    protected $em;

    /**
     * ApiController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/insert/gyro", name="insert_gyro")
     */
    public function insertGyro(Request $request): Response
    {
        $post = $request->request->all();

        $beacon = $this->em->getRepository(Beacon::class)->find($post["beacon"]);

        if($beacon != null){
            $gyroData = new GyroData();
            $gyroData->setBeacon($beacon);
            $gyroData->setX($post["gyroX"]);
            $gyroData->setY($post["gyroY"]);
            $gyroData->setZ($post["gyroZ"]);
            $gyroData->setCreatedAt(new \DateTime("now"));

            $this->em->persist($gyroData);
            $this->em->flush();

            return $this->json([]);
        }

        return $this->json([], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/insert/accel", name="insert_accel")
     */
    public function insertAccel(Request $request): Response
    {
        $post = $request->request->all();

        $beacon = $this->em->getRepository(Beacon::class)->find($post["beacon"]);

        if($beacon != null){
            $accelData = new AccelData();
            $accelData->setBeacon($beacon);
            $accelData->setX($post["accelX"]);
            $accelData->setY($post["accelY"]);
            $accelData->setZ($post["accelZ"]);
            $accelData->setCreatedAt(new \DateTime("now"));

            $this->em->persist($accelData);
            $this->em->flush();

            return $this->json([]);
        }

        return $this->json([], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/insert/status", name="insert_status")
     */
    public function insertStatus(Request $request): Response
    {
        $post = $request->request->all();

        $beacon = $this->em->getRepository(Beacon::class)->find($post["beacon"]);

        if($beacon != null){
            $statusData = new StatusData();
            $statusData->setBeacon($beacon);
            $statusData->setUptime($post["uptime"]);
            $statusData->setCreatedAt(new \DateTime("now"));

            $this->em->persist($statusData);
            $this->em->flush();

            return $this->json([]);
        }

        return $this->json([], Response::HTTP_BAD_REQUEST);
    }
}
