<?php

namespace App\Controller;

use App\Entity\Beacon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(EntityManagerInterface $em): Response
    {
        $beacons = $em->getRepository(Beacon::class)->findByIsEnabled(true);
        return $this->render('dashboard.html.twig', [
            'beacons' => $beacons,
        ]);
    }
}
