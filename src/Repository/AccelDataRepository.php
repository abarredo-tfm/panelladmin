<?php

namespace App\Repository;

use App\Entity\AccelData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccelData|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccelData|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccelData[]    findAll()
 * @method AccelData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccelDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccelData::class);
    }

    // /**
    //  * @return AccelData[] Returns an array of AccelData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccelData
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
