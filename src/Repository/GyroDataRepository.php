<?php

namespace App\Repository;

use App\Entity\GyroData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GyroData|null find($id, $lockMode = null, $lockVersion = null)
 * @method GyroData|null findOneBy(array $criteria, array $orderBy = null)
 * @method GyroData[]    findAll()
 * @method GyroData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GyroDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GyroData::class);
    }

    // /**
    //  * @return GyroData[] Returns an array of GyroData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GyroData
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
