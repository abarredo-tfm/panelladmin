<?php

namespace App\Repository;

use App\Entity\StatusData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatusData|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusData|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusData[]    findAll()
 * @method StatusData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusData::class);
    }
}
