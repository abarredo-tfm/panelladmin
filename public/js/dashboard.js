setInterval(function (){
    $("[data-url]").each(function (index, element){
        updateBeacon($(element));
    });
}, 5000);


function updateBeacon($element){
    var url = $element.data("url");

    $.get({
        url: url
      }).done(function(response) {
        $element.find(".uptime").html(msToTime(response.status.uptime));
        $element.find(".rssi").html(response.status.rssi);
        $element.find(".gyroX").html(response.gyro.x);
        $element.find(".gyroY").html(response.gyro.y);
        $element.find(".gyroZ").html(response.gyro.z);
        $element.find(".accelX").html(response.accel.x);
        $element.find(".accelY").html(response.accel.y);
        $element.find(".accelZ").html(response.accel.z);
        $element.find(".lastUpdate").html(response.status.createdAt);
    });
}

function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
        seconds = Math.floor((duration / 1000) % 60),
        minutes = Math.floor((duration / (1000 * 60)) % 60),
        hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}