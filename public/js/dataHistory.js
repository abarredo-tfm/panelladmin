/**
 * Created by Adrib on 09/12/2020.
 */
$(function () {
    var table = $("#dataHistory").DataTable({
        "responsive": true,
        "autoWidth": false,
        "order": [[0, 'DESC']]
    });

    $('#dataHistory thead tr').clone(true).appendTo('#dataHistory thead');
    $('#dataHistory thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

});