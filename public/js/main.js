/**
 * Created by Adrib on 08/12/2020.
 */
$(function() {
    $("[data-href]").click(function (){
        window.location.href = $(this).data("href");
    });
});